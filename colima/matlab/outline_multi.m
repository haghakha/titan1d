clc;clear all;close all;
load('multi_long_run.mat');
thresh=0.0;
    tobeploted=find(nZH>=thresh);
    numSquare=length(tobeploted);
%     connecSquare=zeros(numSquare,4);
%     verSquare=zeros(numSquare*4,2);
    
% figure()
    p = patch('Faces',connecSquare,'Vertices',verSquare);
    
    cmap=colormap;
    % nonZeroPile=find(pileh(1,:)>0.);%scale and norm the colormap based on non zero pile height
    minh=min(nZH(tobeploted));
    maxh=max(nZH(tobeploted));
    normh=round((nZH(tobeploted)-minh)/(maxh-minh)*length(cmap));
    set(gca,'CLim',[0 length(cmap)]);
    set(p,'FaceColor','flat',...
        'FaceVertexCData',nZH,...
        'CDataMapping','direct',...
        'edgecolor','interp');
    
    axis image;
    
    colorbar('YTickLabel',...
        {'Freezing','Cold','Cool','Neutral',...
        'Warm','Hot','Burning','Nuclear'});
    shading flat;
    picname=sprintf('thresh_%f',thresh);
%     print('-dpng',picname);